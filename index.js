var sentence =
    "Java is a programming language created by James Gosling from Sun Microsystems (Sun) in 1991. The target of Java is to write a program once and then run this program on multiple operating systems. The first publicly available version of Java (Java 1.0) was released in 1995. . Sun Microsystems was acquired by the Oracle Corporation in 2010. Oracle has now the steermanship for Java. In 2006 Sun started to make Java available under the GNU General Public License (GPL). Oracle continues this project called OpenJDK.";
let word = "Java";
let count = wordCount(word, sentence);
document.write(
    "Sentence: " +
        sentence +
        "<br><br>'" +
        word +
        "' word found " +
        count +
        " times<br><br>"
);
function wordCount(word, sentence) {
    let count = 0;
    for (let i = 0; sentence[i] != null; i++) {
        let check_word = "";
        if (sentence[i + 3] != null) {
            check_word =
                sentence[i] +
                sentence[i + 1] +
                sentence[i + 2] +
                sentence[i + 3];
            if (check_word == word) {
                count++;
            }
        }
    }
    return count;
}

let number = window.prompt("Enter Number: ");
document.write("You Entered: " + number + "<br>");
number = highestDigit(number);
document.write(number);

function highestDigit(number) {
    let highest = number[0];
    for (let i = 1; number[i] != null; i++) {
        if (
            number[i] == 0 ||
            number[i] == 1 ||
            number[i] == 2 ||
            number[i] == 3 ||
            number[i] == 4 ||
            number[i] == 5 ||
            number[i] == 6 ||
            number[i] == 7 ||
            number[i] == 8 ||
            number[i] == 9
        ) {
            if (number[i] > highest) {
                highest = number[i];
            }
        } else {
            return "Please Enter Number";
            alert("Please Enter Number");
        }
    }
    return "Highest Digit: " + highest;
}
